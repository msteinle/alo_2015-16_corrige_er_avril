import java.time.*;
import java.util.*;
import java.io.*;

class ReseauAgence
{
	
	ArrayList<Agence> liste = new ArrayList<Agence>();
	
	ReseauAgence(String nomFichier)
	{
		try
		{
			Scanner flux = new Scanner(new File(nomFichier));
			int position = 0;
		
			while ( flux.hasNextLine() )
			{
				Agence agence = creerAgence(flux.nextLine(), position);
				liste.add(agence);
				position ++;
			
			}
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
		
	}

	private Agence creerAgence(String ligne, int position)
	{
		Scanner flux = new Scanner(ligne);
		String denomination = flux.next();
		int NPA = flux.nextInt();
		String Commune = flux.next();
		int jour = flux.nextInt();
		int mois = flux.nextInt();
		int annee = flux.nextInt();
		LocalDate creation = creerLocalDate(jour, mois, annee);
		boolean enTravaux = false;
		if (flux.hasNext())
		{
			enTravaux = true;
		}


		return new Agence(position, denomination, NPA, Commune, creation, enTravaux);
	}

	private LocalDate creerLocalDate(int jour, int mois, int annee)
	{
		return LocalDate.of(annee, mois, jour);
	}


	public String toString()
	{
		return liste.toString();
	}
}
	
