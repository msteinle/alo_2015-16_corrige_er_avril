import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class Agence
{
    int agId;
	String agDenomination;
	int agNPA;
	String agCommune;
	LocalDate agDateOuverture;
	boolean agEnTravaux;


    public Agence(int agenceId,String agenceDenomination,int agenceNPA,String agenceCommune,LocalDate agenceOuverture,boolean agenceEnTravaux)
    {
    	
    	agId = agenceId;
		agDenomination = agenceDenomination;
		agNPA = agenceNPA;
		agCommune = agenceCommune;
    	agDateOuverture = agenceOuverture;
		agEnTravaux = agenceEnTravaux;

    }
    
    public String toString()
    {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy");
		String anneeOuverture = formatter.format(agDateOuverture);

    	String travaux = "";
    	
    	if (agEnTravaux){
    		travaux = "En travaux";
    	}
    	
    	return "L'agence n°" + agId + " " + agDenomination + " est située à " + agCommune + " (" + agNPA + "). Elle a été ouverte en "
    		+ anneeOuverture + ". " + travaux + "\n";
    }
   
}

