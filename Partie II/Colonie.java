import java.util.ArrayList;

class Colonie
{
	ArrayList<Lapin> liste = new ArrayList<Lapin>();

/***** NE PAS MODIFIER !!*/
/* Pour le remplissage de la liste */
/**/
/**/Colonie(String nomFichier)
/**/{
/**/	try
/**/	{
/**/		java.util.Scanner flux = new java.util.Scanner(new java.io.File(nomFichier));
/**/		
/**/		while ( flux.hasNext() )
/**/		{
/**/			Lapin lapin = new Lapin(flux.next(), //nom
/**/									nomType(flux.nextInt()), // type chocolat
/**/									valeurGarni(flux.next()), //garni ou pas
/**/									flux.nextInt(), // position x
/**/									flux.nextInt(), //position y
/**/									flux.nextDouble() //poids en kilo
/**/									);
/**/			liste.add(lapin);
/**/		}
/**/	}
/**/	catch (java.io.FileNotFoundException fnfe)
/**/	{
/**/		fnfe.printStackTrace();
/**/	}
/**/}
/**/	
/**/	
/**/private String nomType(int codeType)
/**/{
/**/	switch (codeType)
/**/	{
/**/		case 1 : return "blanc"; //break; inutile à cause du return
/**/		case 2 : return "au lait"; //"break; inutile à cause du return
/**/		default : return "noir";
/**/	}
/**/}
/**/	
/**/private boolean valeurGarni(String chaineLue)
/**/{
/**/	return chaineLue.equals("oui");
/**/}
/**/	
/**/public String toString()
/**/{
/**/	return liste.toString();
/**/}

	public void afficherPoidsMoyens(){
		
		int nbChocolatBlanc = 0;
		int nbChocolatNoir = 0;
		int nbChocolatLait = 0;
		double totalChocolatBlanc = 0;
		double totalChocolatNoir = 0;
		double totalChocolatLait = 0;
				
		for (int x = 0;x < liste.size();x++ ){
			Lapin lapin = liste.get(x);
			if (lapin.typeChocolat.equals("blanc") ){
				nbChocolatBlanc +=1;
				totalChocolatBlanc  += lapin.poids ;
			} else if (lapin.typeChocolat.equals("noir")){
				nbChocolatNoir += 1;
				totalChocolatNoir  += lapin.poids ;
			} else {
				nbChocolatLait += 1;
				totalChocolatLait  += lapin.poids;
			}
		}
		
		double moyenneChocolatBlanc = totalChocolatBlanc /  nbChocolatBlanc * 1000 ;
		double moyenneChocolatNoir = totalChocolatNoir / nbChocolatNoir * 1000;
		double moyenneChocolatLait = totalChocolatLait / nbChocolatLait * 1000;
		
		if (nbChocolatBlanc != 0){
			System.out.println("Il y a " + nbChocolatBlanc + " lapins en chocolat blanc pesant " + totalChocolatBlanc + " kg.");
			System.out.println("Soit un poids moyen de " + EprPaques.formatterReel(moyenneChocolatBlanc) + " grammes.");
		} else {
			System.out.println("aucun lapin dans la catégorie chocolat blanc");
		}
		
		if(nbChocolatNoir != 0){			
			System.out.println("Il y a " + nbChocolatNoir + " lapins en chocolat noir pesant " + totalChocolatNoir + " kg.");
			System.out.println("Soit un poids moyen de " + EprPaques.formatterReel(moyenneChocolatNoir) + " grammes.");
		} else {
			System.out.println("aucun lapin dans la catégorie chocolat noir");			
		}
		
		if(nbChocolatLait != 0){
			System.out.println("Il y a " + nbChocolatLait + " lapins en chocolat au lait pesant " + totalChocolatLait + " kg.");
			System.out.println("Soit un poids moyen de " + EprPaques.formatterReel(moyenneChocolatLait) + " grammes.");
		} else {
			System.out.println("aucun lapin dans la catégorie chocolat au lait");	
		}
		
	}
	
	public Lapin lapinLePlusLourd(){

		double poidsMax = 0;
		Lapin grosLapin = null;

		for (int x = 0;x < liste.size();x++ ){
			double poidsCourant = liste.get(x).poids;
			if (poidsMax < poidsCourant){
				grosLapin = liste.get(x);
				poidsMax = poidsCourant;
			}
		}

		return grosLapin;
	}
	
	public void afficherToutesLesDistances(){
		for (int x = 0;x < liste.size();x++ ){
			liste.get(x).afficherDistanceAvecLesAutres(this);
		}
		
	}

}