class Lapin
{
/***** NE PAS MODIFIER !!*/
/**/	String nom;
/**/	String typeChocolat ; // soit "blanc", soit "au lait", soit "noir"
/**/	boolean garni; // false si le lapin ne contient rien et true s'il contient des bonbons ou des petits oeufs
/**/	Position coordonnées;
/**/	double poids; //en kilo
/**/	
/**/	Lapin(String nom, String typeChocolat, boolean garni, int x, int y,	double poids)
/**/	{
/**/		this.nom = nom;
/**/		this.typeChocolat = typeChocolat;
/**/		this.garni = garni;
/**/		coordonnées = new Position(x, y);
/**/		this.poids = poids;
/**/	}
/**/	
/**/	int distanceAvec(Lapin autreLapin)
/**/	{
/**/		return coordonnées.distanceAvec(autreLapin.coordonnées);
/**/	}

	public String toString()// à modifier !! (Q1)
	{
		String lapinGarni = "";
		double poidGramme = poids * 1000;
		
		if (garni == true){
			lapinGarni = " (garni)";
		}
		
		return "\n" + nom + " pèse " + poidGramme + " grammes de chocolat " +  typeChocolat + lapinGarni + " et se trouve en position " + coordonnées;
		
	}
	
	public void afficherDistanceAvecLesAutres(Colonie colonie){
		for (int x = 0;x < colonie.liste.size();x++){
			Lapin autreLapin = colonie.liste.get(x);
			if (!nom.equals(autreLapin.nom)){
				int nbdistance = 0;
				nbdistance = distanceAvec(autreLapin);
				System.out.println(nom + " - " + autreLapin.nom + " : " + nbdistance);
			}
		}
	}
	
}

/***** NE PAS MODIFIER !!*/
/**/class Position
/**/{
/**/	int x;
/**/	int y;
/**/	
/**/	Position(int x, int y)
/**/	{
/**/		this.x = x;
/**/		this.y = y;
/**/	}
/**/	
/**/	int distanceAvec(Position autrePosition)
/**/	{
/**/		return Math.abs(x-autrePosition.x)+Math.abs(y-autrePosition.y);
/**/	}
/**/	
/**/	public String toString()
/**/	{
/**/		return "("+x+", "+y+")";
/**/	}
/**/}