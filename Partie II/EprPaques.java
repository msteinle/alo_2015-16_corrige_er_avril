class EprPaques
{
	public static void main (String[] args)
	{
		Colonie colonie = new Colonie("RabbitData.txt");
		
		System.out.println ("Données lues et enregistrées : ");
		System.out.println (colonie.toString());
		
		
				
		// exemple d'utilisation de formatterReel // vous pouvez effacer ou commenter
		System.out.println ();
		System.out.println ();
		System.out.println (EprPaques.formatterReel(235.65)); //va afficher 236 (arrondi)
		
		System.out.println ();
		System.out.println ();
		// ici appel de afficherPoidsMoyens()
		colonie.afficherPoidsMoyens();
		
		
		System.out.println ();
		System.out.println ();
		// ici appel de lapinLePlusLourd()
		// et affichage spécifique des informations sur le lapin le plus lourd
		Lapin grosLapin = colonie.lapinLePlusLourd();
		System.out.println ("Le lapin le plus lourd pèse " + (grosLapin.poids*1000) + " grammes.");
		System.out.println ("Il s'appelle " + grosLapin.nom + " et vous le trouverez en position " + grosLapin.coordonnées);
		
		
		System.out.println ();
		System.out.println ();
		// ici appel de afficherToutesLesDistances()
		colonie.afficherToutesLesDistances();
		
	}
	
/***** NE PAS MODIFIER !!*/
/**/static String formatterReel(double d)
/**/{
/**/	return new java.text.DecimalFormat("0").format(d);
/**/}
/*************************/

}